import { Component, OnInit } from '@angular/core';
import { LoginApiService } from '../login-api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  public userName;

  constructor(
    private router: Router,
    private loginAPI: LoginApiService 
  ) { }

  ngOnInit() {
    if (this.loginAPI.isValid()) {
      this.userName = this.loginAPI.getUserName();
    } else {
      this.router.navigate(['home'])
    }
  }

}
