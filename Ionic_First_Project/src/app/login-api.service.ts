import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginApiService {

  private userName;
  private passWord;
  private valid = false;

  constructor() { }

  /*----- GETTER/SETTER -----*/
  isValid() {
    return this.valid;
  }

  setUserName(_userName) {
    this.userName = _userName;
    this.valid = true;
  }
  getUserName() {
    return this.userName;
  }

  setPassWord(_passWord) {
    this.passWord = _passWord;
  }
  getPassWord() {
    return this.passWord;
  }
}
