import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.page.html',
  styleUrls: ['./projects.page.scss'],
})
export class ProjectsPage implements OnInit {

  public listProjects = [];

  constructor() { }

  ngOnInit() {
    this.listProjects.push({
      name:'Relief Apps',
      description:'Back off app, it\'s a scientist.'
    },
    {
      name:'BetterWork',
      description:'They\'re gonna go full stream.'
    },
    {
      name:'Medoc',
      description:'Oktay, brought the dog!'
    });
  }

}
