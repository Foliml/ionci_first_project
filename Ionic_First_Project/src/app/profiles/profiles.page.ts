import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.page.html',
  styleUrls: ['./profiles.page.scss'],
})
export class ProfilesPage implements OnInit {

  public listDev = [];

  constructor() { }

  ngOnInit() {
    this.listDev.push({
      name:'Woody',
      description:'Back off man, I\'m a scientist.'
    },
    {
      name:'Flinn',
      description:'We\'re gonna go full stream.'
    },
    {
      name:'Jessy',
      description:'Okay, who brought the dog?'
    });
  }

}
