import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginApiService } from '../login-api.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(
    private router: Router,
    private loginAPI: LoginApiService
    ) {}

  ngOnInit() {
  }

  eventSignIn(user, pass) {
    this.loginAPI.setUserName(user);
    this.loginAPI.setPassWord(pass);
    console.log(this.loginAPI.getUserName() + " : " + this.loginAPI.getPassWord());
    this.router.navigate(['profile']);
    console.log("Routing ...");
  }

}
